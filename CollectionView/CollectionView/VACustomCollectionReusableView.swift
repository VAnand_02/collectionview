//
//  VACustomCollectionReusableView.swift
//
//  Created by Vikash Anand on 24/01/20.
//  Copyright © 2020 Vikash Anand. All rights reserved.
//

import UIKit

class VACustomCollectionReusableView: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.myCustomInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.myCustomInit()
    }
    
    func myCustomInit() {
        self.backgroundColor = UIColor.red
    }
}
