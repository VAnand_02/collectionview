//
//  CollectionViewController.swift
//
//  Created by Vikash Anand on 24/01/2020.
//  Copyright © 2020 Vikash Anand. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CollectionViewController : UIViewController, UICollectionViewDelegate {
    
    private let barSize : CGFloat = 44.0
    private let kCellReuse : String = "Cell"
    private let kCellFooterReuse : String = "Footer"
    weak var collectionView: UICollectionView!
    
    private var showFooterView: Bool = true
    
    override func loadView() {
        super.loadView()
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            ])
        self.collectionView = collectionView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Collection View"
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

        self.collectionView.register(VACustomCollectionViewCell.self, forCellWithReuseIdentifier: kCellReuse)
        self.collectionView.register(VACustomCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: kCellFooterReuse)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Tap Me", style: .plain, target: self, action: #selector(showHideFooter))
    }
    
    @objc func showHideFooter() {
        showFooterView = !showFooterView
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func viewWillLayoutSubviews() {
        let frame = self.view.frame
        self.collectionView.frame = CGRect(x:frame.origin.x, y:frame.origin.y + barSize, width:frame.size.width, height:frame.size.height - barSize)
    }
}


extension CollectionViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kCellReuse, for: indexPath) as! VACustomCollectionViewCell
        cell.textLabel.text = String(indexPath.row + 1)
        return cell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 50
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        guard kind == UICollectionView.elementKindSectionFooter else {
            return UICollectionReusableView()
        }
        
        let view : VACustomCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: kCellFooterReuse, for: indexPath as IndexPath) as! VACustomCollectionReusableView
        return view
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let frame : CGRect = self.view.frame
        let margin  = (frame.width - 90 * 3) / 6.0
        return UIEdgeInsets(top:10, left:margin, bottom:10, right:margin)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return showFooterView ? CGSize(width:self.view.frame.width, height:90) : .zero
    }
}
